var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: '🚂 Express' });
});

router.get('/forward', function(req, res, next){
  const forwardTo = eval ('(' + req.query.forwardTo + ')');
  res.redirect(forwardTo.url);
});

module.exports = router;
